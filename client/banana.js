let CANVAS = document.getElementById('game-canvas');

CANVAS.width = window.innerWidth;
CANVAS.height = window.innerHeight;
let ctx = CANVAS.getContext('2d')


let modal = document.getElementById('myModal'),
    btn = document.getElementById("myBtn"),
    span = document.getElementsByClassName("close")[0];

const CARTE_SICILIANE = new Image();
CARTE_SICILIANE.src = 'assets/images/carte_siciliane.png'

const CARD_W = 480,
    CARD_H = 750;

class GameManager {

    constructor(config) {
        this.state = {
            players: config.players,
            giroMorto: config.giroMorto,
            cardType: config.cardType, /* Napoletane/ siciliane*/
            matchPhase: 'distCard',
            score: 0,
            token: 0
        }
        this.serverlisteners();
        this.matchManager();
        this.render();
    }

    setState(s) {
        Object.assign(this.state, s);
        this.render();
        this.matchManager();
    }

    serverlisteners() {

    }

    drawBackground() {
        ctx.beginPath();
        ctx.rect(0, 0, CANVAS.width, CANVAS.height);
        ctx.fillStyle = "#339900";
        ctx.fill();
    }

    drawHandCard() {
        let player = this.state.players[0];
        let cardsInHand = player.cardsInHand
        console.log(cardsInHand)
        for (let i = 0; i < cardsInHand.length; i++) {
            let x = 0 + CARD_W / 3 * i,
                y = 0,
                ox = (cardsInHand[i] % 10) * CARD_W,
                oy = Math.floor(cardsInHand[i] / 10) * CARD_H;
                console.log(oy)
            ctx.drawImage(CARTE_SICILIANE, ox, oy, CARD_W, CARD_H, x, y, CARD_W / 3, CARD_H / 3)
            
        }
    }


    matchManager() {
        switch (this.state.matchPhase) {
            case 'distCard':
                let cards = []
                for (let i = 0; i < 40; i++) {
                    cards.push(i);
                }
                let shuffledDeck = this.shuffleDeck(cards);
                for (let i = 0; i < this.state.players.length; i++) {
                    for (let j = 0; j < 8; j++) {
                        this.state.players[i].cardsInHand.push(shuffledDeck.pop())
                    }
                }
                console.dir(this.state.players)
                this.setState({ matchPhase: 'chiamata' })
                console.dir(this.state)
                break;
            case 'chiamata':
                let self = this;
                socket.on('tokenUpdate', (data) => {
                    alert(data)
                    self.setState({ token: data });
                })
                /*socket.on('scoreUpdate', (data) => {
                    self.setState({ score: data });

              document.getElementById('chiamata-input').value=data
                })
                */
                let players = this.state.players,
                    token = this.state.token;
                if (players[token] === players[0]) {
                    if (players[token].chiamante) {

                        /* TODO: modal chiamata  */
                        modal.style.display = "block";
                        /* passa al servr lo score e dgli di aumentare il token */
                    } else {
                        //socket.emit('cantCall')
                    }
                }
                /* passa al server lo score */
                /* if (this.state.score === 0) {
                     this.setState({ matchPhase: 'distCard' })
                 }*/

                break;
        }
    }


    inviaChiamata() {
        let valore = document.getElementById('chiamata-input').value
        socket.emit('newChiamataScore', valore)

        /* debug */
        alert(valore)
    }

    passaChiamata() {
        modal.style.display = "none";
        //socket.emit('passChiamata')

    }

    turn() {

    }

    render() {
        this.drawBackground();
        this.drawHandCard();
    }

    shuffleDeck(deck) {
        let currentIndex = deck.length,
            temporaryValue,
            randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = deck[currentIndex];
            deck[currentIndex] = deck[randomIndex];
            deck[randomIndex] = temporaryValue;
        }
        return deck;
    }

}

config = {
    players: [new Player(null, []), new Player(null, []), new Player(null, []), new Player(null, []), new Player(null, [])],
    giroMorto: false,
    cardType: 0,

}
let game = new GameManager(config);









// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}