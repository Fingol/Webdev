class GameManager {

    constructor(config) {
        this.state = {
            room: {},
            players: config.players,
            giroMorto: config.giroMorto,
            cardType: config.cardType, /* Napoletane/ siciliane*/
            matchPhase: 'distCard',
            score: 0,
            token: 0
        }
        //this.serverlisteners();
        this.matchManager();
        this.render();

    }

    setState(state) {
        Object.assign(this.state, state);
        this.render();
        this.matchManager();
    }

    init(data) {
        socket.emit('request-game-state', window.registeredRoom)
    }

    matchManager() {
        switch (this.state.matchPhase) {
            case 'distCard':
                let cards = []
                for (let i = 0; i < 40; i++) {
                    cards.push(i);
                }
                let shuffledDeck = this.shuffleDeck(cards);
                for (let i = 0; i < this.state.players.length; i++) {
                    for (let j = 0; j < 8; j++) {
                        this.state.players[i].cardsInHand.push(shuffledDeck.pop())
                    }
                }
                console.dir(this.state.players)
                this.setState({ matchPhase: 'chiamata' })
                console.dir(this.state)
                break;
            case 'chiamata':
                let self = this;
                socket.on('tokenUpdate', (data) => {
                    alert(data)
                    self.setState({ token: data });
                })
                /*socket.on('scoreUpdate', (data) => {
                    self.setState({ score: data });

              document.getElementById('chiamata-input').value=data
                })
                */
                let players = this.state.players,
                    token = this.state.token;
                if (players[token] === players[0]) {
                    if (players[token].chiamante) {

                        /* TODO: modal chiamata  */
                        modal.style.display = "block";
                        /* passa al servr lo score e dgli di aumentare il token */
                    } else {
                        //socket.emit('cantCall')
                    }
                }
                /* passa al server lo score */
                /* if (this.state.score === 0) {
                     this.setState({ matchPhase: 'distCard' })
                 }*/

                break;
        }
    }


    inviaChiamata() {
        let valore = document.getElementById('chiamata-input').value
        socket.emit('newChiamataScore', valore)

        /* debug */
        alert(valore)
    }

    passaChiamata() {
        modal.style.display = "none";
        //socket.emit('passChiamata')

    }

    turn() {

    }

/* sul server */
    shuffleDeck(deck) {
        let currentIndex = deck.length,
            temporaryValue,
            randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = deck[currentIndex];
            deck[currentIndex] = deck[randomIndex];
            deck[randomIndex] = temporaryValue;
        }
        return deck;
    }

    render_GameStatus() {
        let room = this.state.room;
        let target = document.getElementById('game-status'),
            html = '';
        switch (room.gameState.phase) {
            case 'idle':
                html = '<h5>In attesa di giocatori...</h5>'
                break;
            default:

        }
        target.innerHTML = html;
    }


    render() {
        this.render_GameStatus();
        let room = this.state.room
        for (let i = 0; i < room.userInside.length; i++) {
            let user = room.userInside[i]
            let pi = i + 1;
            document.getElementById('player' + pi).innerHTML = `<p>${user.username}</p>`
        }

    }

}

let config = {
    players: [new Player(null, []), new Player(null, []), new Player(null, []), new Player(null, []), new Player(null, [])],
    giroMorto: false,
    cardType: 0,

}

let game = new GameManager(config)

socket.on('update-game-state', (data) => {
    console.log(data)
    game.setState({ room: data })
})



// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}