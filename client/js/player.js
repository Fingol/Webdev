class Player {

    get cardsInHand() {
        return this._cardsInHand;
    }

    get user() {
        return this._user;
    }

    set cardsInHand(value) {
        this._cardsInHand = value;
    }

    set user(value) {
        this._user = value;
    }


    get chiamante() {
        return this._chiamante;
    }

    set chiamante(value) {
        this._chiamante = value;
    }

    constructor(user, cardsInHand) {
        this._user = user;
        this._cardsInHand = cardsInHand;
        this._chiamante = true;
    }


}