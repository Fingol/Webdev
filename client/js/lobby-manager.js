class LobbyManager {

    constructor() {
        this.state = {
            rooms: {}
        };
        this.roomStateListener();
    }


    setState(state) {
        Object.assign(this.state, state);
        this.render();
    }

    roomStateListener() {
        let self = this;
        console.log('listner')
        socket.on('update-room-status', (data) => {
            self.setState({rooms: data});
        })
    }

    render() {

        for (let r = 1; r < 6; r++) {
            let room = this.state.rooms['room' + r];
            let ti = `room${r}-table`
            let target = document.getElementById(ti)
            target.innerHTML = '';
            for (let i = 0; i < room.userInside.length; i++) {
                let user = room.userInside[i],
                    row = target.insertRow(0),
                    cell = row.insertCell(0);
                cell.innerHTML = `<p>${user.username}</p>`;
            }
        }
    }

}

const lobby = new LobbyManager()
