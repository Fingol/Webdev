var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var path = require('path');


const bcrypt = require('bcrypt');

let mysql = require('mysql');

let connection = mysql.createConnection({
    host: "localhost",
    port: 8846,
    user: "root",
    password: "",
    database: "webdevprj"
});

connection.connect(function (err) {
    if (err) throw err;
    console.log(`[DATABASE][INFO] Connesso`);
});

server.listen(3000);


app.use(express.static(path.join(__dirname, '../client')));
app.get('/', function (req, res) {

    res.sendfile(path.join(__dirname, '../client/index.html'));
});


let RoomState = {
    room1: {
        userInside: [],
        gameState: {
            phase: 'idle'
        }
    },
    room2: {
        userInside: [],
        gameState: {
            phase: 'idle'
        }
    },
    room3: {
        userInside: [],
        gameState: {
            phase: 'idle'
        }
    },
    room4: {
        userInside: [],
        gameState: {
            phase: 'idle'
        }
    },
    room5: {
        userInside: [],
        gameState: {
            phase: 'idle'
        }
    },
    room6: {
        userInside: [],
        gameState: {
            phase: 'idle'
        }
    },
};

io.on("connection", (socket) => {
    console.info(`[SOCKET][INFO] Client connesso! [ID=${socket.id}]`);


    socket.on('register-user', (data) => {
        let password = bcrypt.hashSync(data.password, 5);
        let sql = `INSERT INTO users (username, password, level, score, win, lose) VALUES ('${data.username}', '${password}', 1, 0, 0, 0)`;
        connection.query(sql, function (err, result) {
            if (err) {
                socket.emit('registration-error', err);
                console.error(`[DATABASE][ERROR]  ${err}`);
            } else {
                socket.emit('registration-success', 'Registrato con successo');
                console.log("[DATABASE][INFO] Record inserito");
            }

        });
    });

    socket.on('login-user', (data) => {
        let sql = `SELECT * FROM users WHERE username = '${data.username}'`;
        connection.query(sql, function (err, rows) {
            if (err)
                socket.emit('login-error', err);
            else if (!rows.length) {
                socket.emit('login-error', 'Username non trovato');
            }
            else if (!bcrypt.compareSync(data.password, rows[0].password)) {

                socket.emit('login-error', 'Password non corretta');
            } else {
                socket.emit('login-success', rows[0]);
            }
        });
    });


    /* LOBBY */

    function updateRoomStatus() {
        socket.emit('update-room-status', RoomState);
    }

    socket.on('request-room-status', () => {
        updateRoomStatus();
    });

    socket.on('request-join-room', (data) => {
        if (RoomState[data.room].userInside.length < 5) {
            console.log(`REQUEST JOIN ROOM ${data.room}`)
            socket.join(data.room)
            RoomState[data.room].userInside.push(data.user);
            socket.emit('request-confirmed', data.room);
        }
        socket.emit('update-room-status', RoomState);
    });


    /* GAME */

    function emitUpdate(data, room) {
        socket.emit('update-game-state', data)
        socket.in(room).emit('update-game-state', data)
    }

    socket.on('request-game-state', (data) => {
        if (RoomState[data].userInside.length === 5) {
            RoomState[data].gameState.phase = 'start'
        }
        console.log(`[GAME] ${socket.id} requested game state`)
        emitUpdate(RoomState[data], data)
    });


    // when socket disconnects, remove it from the list:
    socket.on("disconnect", () => {
        console.info(`[SOCKET][INFO] Client disconnesso! [id=${socket.id}]`);
    });
});
